Laser triangulation method
==========================

Laser triangulation is a method of reconstructing 3D shape of physical objects
to the form of point cloud.
It uses laser light projected on the object at known angle and observed by
digital camera.

Due to perspective projection the poisition of laser profile depicted
on acquired digital image is shifted depending on the height of the object
on which it falls.

Image of projected laser light acquired by calibrated camera at known angle
may be used to measure the shape of the object
on which the light was projected.

Laser triangulation method is successfully used in industrial quality control
for object digitisation for over 30 years
:cite:`Curless-1995-TriangulationSpacetimeAnalysis`.
It still plays significant role in today's industry -especially in the area
of quality inspection.

Measurement devices implementing laser triangulation method typically used
laser line projectors  -see Fig. :ref:`modus_operandi_LT`
Such devices are called *laser profilometers*, as every image acquired by such
them provides a series of 3D coordinates of a series of points along the
laser light profile -not just a single point.

This Python package focues on measurements with use of laser line.

.. _modus_operandi_LT:
.. figure:: modus_operandi_LT.svg
   :width: 600
   :alt: Diagram showing principle of laser triangulation method
   :name: fig:SchematMTL

   Principles and components of laser profilometer

The image shows:

   1. camera
   2. laser projector
   3. object to be digitised
   4. sheet (plane) of laser light
   5. laser profile illuminating surface of the object and its surrounding
   6. image of laser profile registered by the camera (1)

The main advantages of this technique are:

* contactless measurements

* relatively fast 3D measurement process

* robustness against external light

* constantly increasing resolution and speed with improving
  digital imaging techniques, processors and data transfer buses

Unfortunately, just like in most vision systems, it is susceptible to many
factors affecting accuracy, and sometimes even preventing measurement.


Bibliography
------------

.. bibliography:: ../library.bib

