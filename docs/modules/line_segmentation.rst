Line segmentation module
------------------------

.. automodule:: laser_triangulation.line_segmentation
   :members:
