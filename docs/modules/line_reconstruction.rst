Line reconstruction module
--------------------------

.. automodule:: laser_triangulation.line_reconstruction
   :members: