Laser triangulation package documentation
+++++++++++++++++++++++++++++++++++++++++

.. toctree::
   :maxdepth: 2
   :caption: Contents

   laser_triang/laser_triang_intro
   package
   modules/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


License
=======

This project (including documentation) is distributed under
`GNU General Public License v3.0 or later
<https://spdx.org/licenses/GPL-3.0-or-later.html>`_ (GPLv3+) license.
