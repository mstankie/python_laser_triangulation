#!/usr/bin/env python

# This file is part of python_laser_triangulation package.

# python_laser_triangulation is free software:
# you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# python_laser_triangulation is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

""" python_laser_triangulation setup script

:author: Maciej Stankiewicz <maciej@stankiewicz.pro>
:copyright: Copyright © 2013, 2020 Maciej Stankiewicz
:license: GPLv3+

"""

import setuptools

with open("README.md", "r") as fh:
    README = fh.read()

setuptools.setup(
    name="python_laser_triangulation",
    version="0.0",
    author="Maciej Stankiewicz",
    author_email="maciej@stankiewicz.pro",
    copyright="Copyright © 2013, 2020 Maciej Stankiewicz",
    description="Calibrating and processing laser triangulation images",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mstankie/python_laser_triangulation",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent"
    ],
    python_requires='>=3.6',
    install_requires=[
        "opencv-python",
        "pytest",
    ],
)
