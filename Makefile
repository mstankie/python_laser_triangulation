init:
	pip install --no-cache-dir -r requirements.txt

update-requirements:
	pip freeze > requirements.txt

stylecheck:
	flake8 laser_triangulation/*.py

lint:
	pylint laser_triangulation/*.py

securitycheck:
	bandit -r laser_triangulation/*.py

test:
	pytest --cov=laser_triangulation tests/ | tee coverage_report.txt

build:
	python setup.py sdist bdist_wheel

.PHONY: init update-requirements stylecheck lint securitycheck test build