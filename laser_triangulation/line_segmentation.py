#!/usr/bin/env python

# This file is part of python_laser_triangulation package.

# python_laser_triangulation is free software:
# you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# python_laser_triangulation is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

""" Line segmentation module

Line segmentation algorithms for laser triangulation

:author: Maciej Stankiewicz <maciej@stankiewicz.pro>
:copyright: Copyright © 2013, 2020 Maciej Stankiewicz
:license: GPLv3+

"""

import numpy
import cv2


class LineFinderCoG:
    """ Segments laser line using centre-of-gravity algorithm

    Given grayscale traiangulation image with one laser line,
    provides coordinates of points along the laser profile.

    Points are returned in image coordinates in pixel units.
    Horizontal coordinates (u) are always integer;
    vertical coordinates (v) are estimated with subpixel precision thanks to
    using entre-of-gravity algorithm:

    * First values of every pixel is thresholded with threshold value math:`T`:
        .. math::
            I_{u,v} = \\left\\{\\begin{matrix}
            I_{u,v}<T_I \\rightarrow 0\\\\
            I_{u,v} \\geq T_I \\rightarrow I_{u,v}
            \\end{matrix}\\right.

    * The algorithm uses pixel intensity values along each image column
      to estimate the centrepoint of laser line
      .. math: u_l = \\frac{\\sum_{u=0}^n u \\cdot I_u}{\\sum_{u=0}^n u}

    :raises ValueError: in case of inconsistent data
    :return: image coordinates [px] of points along laser line
    :rtype: numpy.array
    """

    def __init__(self, threshold: int, image_size: tuple, smoothing=1) -> None:

        self._threshold = None
        self.threshold = threshold
        self._image_size = image_size
        self._build_internal_images()
        self._smoothing = None
        self.smoothing = smoothing

    def _build_internal_images(self) -> None:

        indexes = range(self._image_size[0])
        self._index_image = numpy.array(indexes, numpy.int32)
        self._index_image = numpy.repeat(self._index_image,
                                         self._image_size[1])
        self._index_image.shape = self._image_size

    @property
    def threshold(self) -> int:
        """Image threshold value
        Threshold value is used to eliminate influence of low-value pixels on
        line segmentation process.

        Refer to value math:`T` in algorithm description in:
        :class:`LineFinderCoG`

        :return: Current threshold value
        :rtype: int
        """

        return self._threshold

    @threshold.setter
    def threshold(self, value: int) -> None:

        if not 0 <= value < 256:
            raise ValueError("Threshold value must be between 0 and 255.")
        self._threshold = value

    @property
    def smoothing(self) -> int:
        """Image smoothing kernel size

        Laser line on the triangulation image is sometimes uneven due to
        surface properties, speckles or other influences.
        In order to make the line segmetation robust against that,
        triangulation image may be first smoothed by applying Gaussian blur.

        This value is the size of Gaussian smoothing kernel.

        :return: Gaussian smoothing kernel size
        :rtype: int
        """

        return self._smoothing[0]

    @smoothing.setter
    def smoothing(self, value: int) -> None:

        if not 1 <= value <= 30:
            raise ValueError("Smoothing kernel size must be between 0 and 30.")
        self._smoothing = (value, value)

    def __call__(self, image: numpy.array) -> numpy.array:
        """ Segments laser triangulation image.

        Finds 2D coordinates of points along laser line image using CoG algo.

        :param image: Triangulation image with single bright laser line
        :type image: numpy.array
        :raise ValueError: If image is not of the shape defined in class ctor
                           or not 8-bit grayscale.
        :return: Detected line points in image coordinates [u,v]
        :rtype: numpy.array
        """

        if image.dtype != numpy.uint8:
            raise ValueError("Only 8-bit images handled")

        if self._image_size != image.shape:
            if len(image.shape) != 2:
                raise ValueError("Only 1-channel (grayscale) images handled")
            raise ValueError(
                f"Only images of {self._image_size} shape handled.")

        if self.smoothing > 1:
            blurred = cv2.GaussianBlur(image, self._smoothing, 0)
        else:
            blurred = image

        _, thresholded = cv2.threshold(blurred, self._threshold, 0,
                                       cv2.THRESH_TOZERO)
        threshd_sums = numpy.sum(thresholded, axis=0, dtype=numpy.float32)

        indexed = thresholded * self._index_image
        indexed_sums = numpy.sum(indexed, axis=0, dtype=numpy.float32)

        rows_with_line = numpy.nonzero(numpy.sum(thresholded, axis=0))

        indexed_sums = indexed_sums[rows_with_line]
        threshd_sums = threshd_sums[rows_with_line]

        # rows_with_line will be used as u coordinates for points
        rows_with_line = rows_with_line[0].astype(numpy.float32)

        line_positions = indexed_sums / threshd_sums
        line_positions = numpy.vstack((rows_with_line, line_positions)).T

        return line_positions
