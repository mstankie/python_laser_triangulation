#!/usr/bin/env python

# This file is part of python_laser_triangulation package.

# python_laser_triangulation is free software:
# you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# python_laser_triangulation is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

""" Module for 3D coordinate reconstruction from triangulation images.

:author: Maciej Stankiewicz <maciej@stankiewicz.pro>
:copyright: Copyright © 2013, 2020 Maciej Stankiewicz
:license: GPLv3+
"""

import numpy
import cv2


class Reconstruction3D:
    """3D reconstruction for points identified in triangulation images.

        Default Reconstruction3D builds identity transformation.
        In order to calculate correct coordinates, one needs to
        provide appropriate calibration data:
        - camera matrix,
        - distortion coefficients,
        - laser plane homography,

        :iparam camera_matrix: Lens calibration parameters
        :itype camera_matrix: numpy.array
        :iparam dist_coefs: Lens distortion coefficients
        :itype dist_coefs: numpy.array
        :iparam laser_homogr: Image-Laser homography matrix
        :itype laser_homogr: numpy.array
    """

    def __init__(self):

        self._camera_matrix = numpy.eye(3, dtype=numpy.float32)
        self._dist_coefs = numpy.array([0.0, 0.0, 0.0, 0.0, 0.0],
                                       numpy.float32)
        self._laser_homogr = numpy.eye(3, dtype=numpy.float32)

    @property
    def camera_matrix(self) -> numpy.array:
        """Camera matrix used to reconstruct image objects.

        Camera matrix may be estimated in OpenCV camera calibration
        process, as described in OpenCV documentation.

        :return: Camera matrix
        :rtype: numpy.array [3x3]
        """

        return self._camera_matrix

    @camera_matrix.setter
    def camera_matrix(self, mtx: numpy.array) -> None:

        if mtx.shape != (3, 3):
            raise ValueError("Camera matrix must be 3x3")
        self._camera_matrix = mtx

    @property
    def dist_coefs(self) -> numpy.array:
        """Distortion coefficients used to correct point cordinates

        Distortion coefficients may be estimated in OpenCV camera calibration
        process, as described in OpenCV documentation.

        :return: Distortion coefficients
        :rtype: numpy.array (5, )
        """

        return self._dist_coefs

    @dist_coefs.setter
    def dist_coefs(self, coefs: numpy.array) -> None:

        if len(coefs.shape) != 2 \
           or coefs.shape[1] != 1 \
           or not 3 <= coefs.shape[0] <= 7:
            raise ValueError("Distortion coefficients must be 3 to 7 numbers")
        self._dist_coefs = coefs

    @property
    def laser_homogr(self) -> numpy.array:
        """Laser plane homography

        Laser plane homography defines 2D transformation from
        image points to real world 2D coordinates in the laser plane.

        :return: Laser homograpy matrix
        :rtype: numpy.array [3x3]
        """

        return self._laser_homogr

    @laser_homogr.setter
    def laser_homogr(self, mtx: numpy.array) -> None:

        if mtx.shape != (3, 3):
            raise ValueError("Homography matrix for 2D points must be 3x3")
        self._laser_homogr = mtx

    def _undistort(self, image_points: numpy.array) -> numpy.array:

        assert len(image_points.shape) == 2 and image_points.shape[1] == 2, \
            "_undistort requires points in standard 2D coordinates (n x 2)"

        number_of_points = image_points.shape[0]

        # OpenCV functions require special coordinates layout
        image_points.shape = (number_of_points, 1, 2)

        # `undistortPoints` function takes in image points in pixel coordinates
        # and returns the coordinates of these with no distortion.
        # If the `P` argument is not provided the result coords are metric with
        # (0, 0) point in the camera's principal point.
        # In order to get the result in same coords, same `camera_matrix` must
        # be given as `P`.
        undistorted_points = cv2.undistortPoints(image_points,
                                                 self._camera_matrix,
                                                 self._dist_coefs,
                                                 P=self._camera_matrix)

        return undistorted_points

    def _reconstruct(self, image_points: numpy.array) -> numpy.array:

        # expected OpenCV indexing format [n, 1, 2]
        assert image_points.shape[1:] == (1, 2)

        reconstructed_points = cv2.perspectiveTransform(image_points,
                                                        self._laser_homogr)

        return reconstructed_points

    def __call__(self, image_points: numpy.array) -> numpy.array:
        """Reconstructs 2D image points to 2D coordinates in laser plane space

        Laser triangulation uses grayscale images of laser line projected onto
        scanned object.
        The goal is to reconstruct virtual 3D model of the object.

        Laser line is identified in the triangulation images.
        This provides 2D coordinates of points along the image of line.
        In order to obtain real world (metric) coordinates of the points along
        the line, one needs to reconstruct them.

        This functor uses calibration data:
        - camera matrix,
        - distortion coefficients,
        - laser plane homography,
        to calculate the 3D coordinates for 2D image points provided.

        :param image_points: 2D points found in triangulation image.
        :type image_points: numpy.array
        :return: 3D coordinates of reconstructed points in laser plane space
        :rtype: numpy.array
        """

        undistorted_points = self._undistort(image_points)

        reconstructed_points = self._reconstruct(undistorted_points)

        # add Z-coordinate as the result points are 3D
        reconstructed_points = numpy.dstack((
            reconstructed_points,
            numpy.zeros((reconstructed_points.shape[0], 1))
            ))

        return reconstructed_points
