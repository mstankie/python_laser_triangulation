FROM python:3.8-slim-buster

LABEL maintainer="Maciej Stankiewicz"
LABEL maintainer-email="maciej@stankiewicz.pro"
LABEL description="Lightweight image for testing OpenCV-based Python code"

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev && \
    apt-get install -y libsm6 libxext6 libxrender-dev

RUN pip3 install --no-cache-dir opencv-python flake8 mypy pylint

ADD requirements.txt /

RUN pip3 install --no-cache-dir -r requirements.txt

