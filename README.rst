Laser triangulation package
===========================

This package provides basic image processing and 3D data reconstruction
algorithms for implementing custom laser triangulation setups
using Python programming language.

Image processing algorithms for production systems shall be extremely efficient
and robust to ensure fast and stable operation.
Industrial laser triangulation sensors use very efficient
line segmentation algorithms implemented in FPGA.

This package is intended for educational and experimental purposes.
The emphasis is put on code readability rather than on optimisation
-e.g. on specific-device like FPGA or GPGPUs.

Some components -like calibration, however, may be applicable even for
optimised implementation.


Modules
-------

This package contains the following modules:

laser_triangulation
    Components required to build laser triangulation image processing chain.
    It starts from segmenting laser line on raw images, and then provides
    classes to convert the image points along laser line on the image to
    3D real-world coordinates of the profile and then to reconstruct the whole
    geometry of the scanned scene.

laser_calibration **[not available yet]**
    Components and ready to use application to find the mapping between
    2D coordinates of points in triangulation image and the corresponding
    metric coordinates of points illuminated by the sheet of laser light.

lens_calibration **[not available yet]**
    Components and ready to use application to estimate the distortion
    introduced by imaging system to the image. This is essential in order
    to accurately reconstruct the 3D coordinates of points scanned using
    most of laser triangulation setups.


Installation
------------

This is a Python package; it requires Python version 3.6 or higher.
Please refer to `Python documentation
<https://wiki.python.org/moin/BeginnersGuide/Download>`_
on how to install Python interpreter in your system.

This package is published on `The Python Package Index <https://pypi.org/>`_.
One can install it using PIP: :code:`pip install python_laser_triangulation`.


Usage
-----

In order to start working with the package, install the requirements:
:code:`make init`
This will install all required dependencies into you environment.
Using `virtualenv <https://virtualenv.pypa.io/en/latest/>`_ is highly recommended.


*Command ``make install`` and the calibration applications
are not available yet*


Running tests and checks
------------------------

check syntax
    :code:`make lint`

check code style
    :code:`make stylecheck`

check potential security issues
    :code:`securitycheck`

run tests
    :code:`make test`


.. start-of-readme-only-content


Documentation
-------------

Package documentation is available at
https://mstankie.gitlab.io/python_laser_triangulation


Contribution
------------

If you see a bug or a way to improve the project, please add your `issue report
<https://gitlab.com/mstankie/python_laser_triangulation/-/issues>`_.


License
-------

This project (including documentation) is distributed under
`GNU General Public License v3.0 or later
<https://spdx.org/licenses/GPL-3.0-or-later.html>`_ (GPLv3+) license.
