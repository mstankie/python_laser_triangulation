#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Tests for Reconstruction3D class """

import os

import pytest

import numpy
import cv2

from laser_triangulation.line_reconstruction import Reconstruction3D

# pylint: disable=invalid-name
# pylint: disable=protected-access


_TEST_DATA_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), 'data')


def test_line_reconstruction_class_ctor():
    """ Line reconstruction created with correct values.

        It shall be constructed with parameters providing identity transform.
    """

    reconstr = Reconstruction3D()

    assert numpy.array_equal(reconstr.camera_matrix,
                             numpy.eye(3, dtype=numpy.float32))
    assert numpy.array_equal(reconstr.dist_coefs,
                             numpy.array([0.0, 0.0, 0.0, 0.0, 0.0],
                                         numpy.float32))
    assert numpy.array_equal(reconstr.laser_homogr,
                             numpy.eye(3, dtype=numpy.float32))

    test_points = numpy.array([[12, 13], [14, 15]], numpy.float32)
    reconstructed_points = reconstr(test_points)
    expected_points = numpy.array([[[12, 13, 0]], [[14, 15, 0]]],
                                  numpy.float32)
    assert numpy.array_equal(reconstructed_points, expected_points)


@pytest.mark.datafiles(os.path.join(_TEST_DATA_DIR, "camera_matrix.xml"))
def test_reading_real_camera_matrix(datafiles):
    """ Camera matrix saved from calibration shall be handled """

    path = str(datafiles.listdir()[0])
    cv_file = cv2.FileStorage(path, cv2.FILE_STORAGE_READ)
    camera_matrix = cv_file.getNode("camera_matrix").mat()
    cv_file.release()

    reconstr = Reconstruction3D()
    reconstr.camera_matrix = camera_matrix

    assert not numpy.array_equal(reconstr.camera_matrix,
                                 numpy.eye(3, dtype=numpy.float32))


@pytest.mark.datafiles(os.path.join(_TEST_DATA_DIR,
                                    "distortion_coefficients.xml"))
def test_reading_real_distortion_coefficients(datafiles):
    """ Distortion coefficients saved from calibration shall be handled """

    path = str(datafiles.listdir()[0])
    cv_file = cv2.FileStorage(path, cv2.FILE_STORAGE_READ)
    distortion_coefficients = cv_file.getNode("distortion_coefficients").mat()
    cv_file.release()

    reconstr = Reconstruction3D()
    reconstr.dist_coefs = distortion_coefficients

    assert not numpy.array_equal(reconstr.dist_coefs,
                                 numpy.eye(3, dtype=numpy.float32))


@pytest.mark.datafiles(os.path.join(_TEST_DATA_DIR,
                                    "homography.xml"))
def test_reading_real_homography(datafiles):
    """ Homography saved from calibration shall be handled """

    path = str(datafiles.listdir()[0])
    cv_file = cv2.FileStorage(path, cv2.FILE_STORAGE_READ)
    homography = cv_file.getNode("homography").mat()
    cv_file.release()

    reconstr = Reconstruction3D()
    reconstr.laser_homogr = homography

    assert not numpy.array_equal(reconstr.laser_homogr,
                                 numpy.eye(3, dtype=numpy.float32))


def test_reconstructs_projected_points():
    """ Function shall reconstruct points projected by inverse transform.

    Number of sample points in laser plane are picked randomly.

    Reconstruction parameters are hardcoded (based on real calibration):
    - homography
    - distortion_coefficients
    - camera_matrix

    The laser_points are first processed by inverse transformation to
    reconstruction 3D (projection), using the parameters named above.

    Then the projected points are processed with reconstruction, which
    shall return points very close to laser_points.
    This is checked by numpy.allclose.

    """

    homography = numpy.array([[0.137544, -0.000788, -86.811935],
                              [0.000294, 0.213846, -140.398224],
                              [-0.000002, -0.000470, 1.000000]])

    inverse_homography = numpy.linalg.inv(homography)

    distortion_coefficients = numpy.array([-0.236422, 0.265309,
                                           -0.001373, -0.000411])
    distortion_coefficients = distortion_coefficients.reshape((4, 1))

    camera_matrix = numpy.array([[1617.067, 0000.000, 639.794],
                                 [0000.000, 1619.390, 517.565],
                                 [0000.000, 0000.000, 001.000]])

    # Pick number of points in laser-plane space (metric coords)
    laser_points = numpy.array([[[-12., -13.]],
                                [[-40., 15.]],
                                [[4., 5.]],
                                [[40., 50.]]])

    undistorted_points = cv2.perspectiveTransform(laser_points,
                                                  inverse_homography)

    # convert to 3D (shape [n x 1 x 3])
    undistorted_points = cv2.convertPointsToHomogeneous(undistorted_points)

    # convert to metric-sensor coordinates
    undistorted_points[:, :, 0] -= camera_matrix[0, 2]  # move to principal pt
    undistorted_points[:, :, 0] /= camera_matrix[0, 0]  # pixel to millimeter
    undistorted_points[:, :, 1] -= camera_matrix[1, 2]
    undistorted_points[:, :, 1] /= camera_matrix[1, 1]

    rvec = numpy.array([0., 0., 0.])
    tvec = numpy.array([0., 0., 0.])

    image_points, _ = cv2.projectPoints(undistorted_points, rvec, tvec,
                                        camera_matrix,
                                        distortion_coefficients)
    image_points = image_points.reshape((-1, 2))  # standard 2D coords required

    reconstr = Reconstruction3D()
    reconstr.laser_homogr = homography
    reconstr.dist_coefs = distortion_coefficients
    reconstr.camera_matrix = camera_matrix

    result_points = reconstr(image_points)

    # result points are in 3D coordinates (Z=0)
    # converting to match the source_points shape
    non_zero_coord_z, _ = numpy.nonzero(result_points[:, :, 2])
    assert non_zero_coord_z.size == 0

    result_points = result_points[:, :, :2]

    assert numpy.allclose(result_points, laser_points)
