#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Tests for LineFinderCoG class """

import pytest

import numpy
import cv2

from laser_triangulation.line_segmentation import LineFinderCoG

# pylint: disable=invalid-name
# pylint: disable=protected-access


def _point_line_distance(P1: numpy.array, P2: numpy.array,
                         Q: numpy.array) -> float:
    """Calculate distance from point Q to a line defined P1 and P2

    line l: x = P1 + t * n
    where n: n = (P2 - P1) / | P2 - P1 | = vL / | vL |

    having vector v between Q and A being the closest point from Q lying on l:
    v = P1 - Q

    distance: d = || v - (v dot n) * n ||

    :param P1: First point on the line
    :type P1: numpy.array
    :param P2: Second point on the line; must not be equal P1
    :type P2: numpy.array
    :param Q: The point at a distance from the line
    :type Q: numpy.array
    :return: Distance between Q and line P1-P2
    :rtype: float
    """

    if P1.shape != (1, 2):
        raise ValueError("P1 must be (1, 2) shape")
    if P2.shape != (1, 2):
        raise ValueError("P2 must be (1, 2) shape")
    if Q.shape[1] != 2 or len(Q.shape) != 2:
        raise ValueError("Q must be array of 2D points")

    vL = P2 - P1  # vector P2 - P1 -parallell to line l
    len_vL = (vL**2).sum()**0.5  # length of vL
    n = vL / len_vL  # normal vector parallel to line l

    v = P1 - Q

    d = v - numpy.dot(v, n.T) * n
    d = numpy.linalg.norm(d, axis=1)

    return d


def test_line_finder_ctor():
    """ Line finder created with correct values """

    lf = LineFinderCoG(10, (20, 30), 5)

    assert lf._threshold == 10
    assert lf._image_size == (20, 30)
    assert lf._smoothing == (5, 5)


def test_default_smoothing_ctor():
    """ Line finder created with default smootihing """

    lf = LineFinderCoG(10, (20, 30))
    assert lf._smoothing == (1, 1)


def test_only_8bit_1ch_images_accepted():
    """ Line finder handles 1-channel 8-bit images only """

    lf = LineFinderCoG(10, (5, 5))

    with pytest.raises(ValueError) as error:
        lf(numpy.zeros((5, 5), numpy.uint16))
    assert "Only 8-bit images" in str(error.value)

    with pytest.raises(ValueError) as error:
        lf(numpy.zeros((5, 5), numpy.float))
    assert "Only 8-bit images" in str(error.value)

    with pytest.raises(ValueError) as error:
        lf(numpy.zeros((5, 5, 3), numpy.uint8))
    assert "Only 1-channel" in str(error.value)


def test_smoothing_only_in_range():
    """ Smoothing must be between 1 and 30 """

    with pytest.raises(ValueError) as error:
        _ = LineFinderCoG(10, (20, 30), 200)
    assert "Smoothing kernel size must be between" in str(error.value)

    with pytest.raises(ValueError) as error:
        lf = LineFinderCoG(10, (20, 30))
        lf.smoothing = 300
    assert "Smoothing kernel size must be between" in str(error.value)


def test_threshold_only_in_range():
    """ Threshold must be between 0 and 255 """

    with pytest.raises(ValueError) as error:
        _ = LineFinderCoG(-5, (20, 30))
    assert "Threshold value must be between" in str(error.value)

    with pytest.raises(ValueError) as error:
        _ = LineFinderCoG(500, (20, 30))
    assert "Threshold value must be between" in str(error.value)

    with pytest.raises(ValueError) as error:
        lf = LineFinderCoG(10, (20, 30))
        lf.threshold = 300
    assert "Threshold value must be between" in str(error.value)


def test_internal_images():
    """ Internal images must be of certain shape and values """

    lf = LineFinderCoG(10, (20, 30))

    assert lf._index_image.shape == lf._image_size

    assert lf._index_image[0, 0] == 0
    assert lf._index_image[0, 29] == 0
    assert lf._index_image[19, 0] == 19
    assert lf._index_image[19, 29] == 19


def test_only_images_with_matching_size_accepted():
    """ Images must have corect size specified in class ctor """

    find_line = LineFinderCoG(5, (10, 20))

    image = numpy.zeros((30, 40), numpy.uint8)

    with pytest.raises(ValueError) as error:
        _ = find_line(image)

    assert "Only images" in str(error.value)


def test_line_detection():
    """ Detects straight line on syntetic triangulation image """

    IMAGE_SIZE = (10, 20)

    find_line = LineFinderCoG(5, IMAGE_SIZE)

    image = numpy.zeros(IMAGE_SIZE, numpy.uint8)

    P1 = (0, 3)
    P2 = (20, 7)

    cv2.line(image, P1, P2, 128, 1)
    image = cv2.GaussianBlur(image, (1, 3), 2)

    points = find_line(image)

    dists = _point_line_distance(numpy.array(P1).reshape((1, 2)),
                                 numpy.array(P2).reshape((1, 2)),
                                 points.reshape((-1, 2)))

    assert all(dists < 1.0)


def test_line_detection_returns_only_existing_points():
    """ Returns values only for colums having line """

    IMAGE_SIZE = (10, 20)

    find_line = LineFinderCoG(5, IMAGE_SIZE)

    image = numpy.zeros(IMAGE_SIZE, numpy.uint8)

    P1 = (4, 3)  # line starts in 4th column (present in 4th column)
    P2 = (16, 7)  # line ends in 16th column (present in 16th column)
    # line is stretched along 13 columns

    cv2.line(image, P1, P2, 128, 1)

    points = find_line(image)

    assert len(points) == 13


def test_line_detection_ignores_values_below_threshold():
    """ Ignores values below threshold """

    IMAGE_SIZE = (15, 20)
    THRESHOLD = 20

    find_line = LineFinderCoG(THRESHOLD, IMAGE_SIZE)

    image = numpy.zeros(IMAGE_SIZE, numpy.uint8)

    P1 = (0, 3)
    P2 = (20, 7)

    # line above threshold
    cv2.line(image, P1, P2, THRESHOLD * 2, 1)
    # dark line below threshold
    cv2.line(image, (3, 10), (16, 13), THRESHOLD / 2, 1)

    points = find_line(image)

    # check if only the line above thresold is detected
    dists = _point_line_distance(numpy.array(P1).reshape((1, 2)),
                                 numpy.array(P2).reshape((1, 2)),
                                 points.reshape((-1, 2)))

    assert all(dists < 1.0)
